let node = {};
let style = document.createElement('style');
style.innerHTML = '.node { border: 5px solid red; }';
document.getElementsByTagName('head')[0].appendChild(style);

function createItem(selector, style, text, ...childs) {
    let action = text.replace(/ /g, '_') +selector;
    let tempEl = document.createElement(selector) || 'div';
    tempEl.style = style || '';
    tempEl.innerText = selector !== 'div' || selector !== 'form' ? text : '';
    tempEl.classList.add(`search-${action.toLowerCase()}`);
    childs.forEach(child => {
    tempEl.appendChild(child);
    });
    return tempEl;
}

headerTitle = createItem('h2', 'font-size:16px', 'Search node element') ;
closeBtn = createItem('button', '', 'x');
searchInput = createItem('input', '', '');
searchBtn = createItem('button','', 'Search');
prevBtn = createItem('button', '', 'Prev');
nextBtn = createItem('button', '', 'Next');
parentBtn = createItem('button', '', 'Parent');
childBtn = createItem('button', '', 'Children');
buttonSection = createItem('div', ' display: flex;justify-content: space-between;', '', prevBtn, nextBtn, parentBtn, childBtn);
searchSection = createItem('form', ' display: flex;justify-content: space-between;margin-bottom: 10px;', '', searchInput, searchBtn);
searchFormHeader = createItem('div', ' display: flex;justify-content: space-between;align-items: flex-end;margin-bottom: 10px;', '', headerTitle, closeBtn);
searchForm = createItem('div',
                        'position: fixed;left: 79%;padding: 0 10px;width: 370px;min-height:160px;background-color: gray;display: flex;flex-direction: column; z-index:5000',
                        '', searchFormHeader, searchSection, buttonSection);
class SelectorForm extends HTMLElement {  
    constructor() {
        super();
        this.$shadow = this.attachShadow({mode: 'open'});
        this._text = createItem('p', 'color:red;font-size:14px', '');
    } 
    connectedCallback() {
        let form = createItem('div', '', '', searchForm);
        this.$shadow.innerHTML = form.innerHTML;
        this.$shadow.addEventListener( 'click', this );
        this.getEl('.search-prevbutton').disabled = true;
        this.getEl('.search-nextbutton').disabled =  true;
        this.getEl('.search-childrenbutton').disabled = true;
        this.getEl('.search-parentbutton').disabled = true;
        let formDiv =  this.getEl('.search-div');
        formDiv.onmousedown = function(event) {
            let shiftX = event.clientX - formDiv.getBoundingClientRect().left;
            let shiftY = event.clientY - formDiv.getBoundingClientRect().top;
            moveAt(event.clientX, event.clientY);
            function moveAt(pageX, pageY) {
                formDiv.style.left = pageX - shiftX + 'px';
                formDiv.style.top = pageY - shiftY + 'px';
                document.body.style.cursor = 'move';
            }
            function onMouseMove(event) {
                moveAt(event.clientX, event.clientY);
            }
            document.addEventListener('mousemove', onMouseMove);
            formDiv.onmouseup = function() {
                document.removeEventListener('mousemove', onMouseMove);
                formDiv.onmouseup = null;
                document.body.style.cursor = 'default';
            };
        };
            formDiv.ondragstart = function() {
            return false;
        };
    }
    disconnectedCallback() {
        node.style = 'border:none';    
    }
    getEl(selector) {
        return this.$shadow.querySelector(selector);
    }
    checkNode(node) {
        this.getEl('.search-prevbutton').disabled = !node.previousElementSibling;
        this.getEl('.search-nextbutton').disabled = !node.nextElementSibling;
        this.getEl('.search-childrenbutton').disabled = !node.children[0];
        this.getEl('.search-parentbutton').disabled = !node.parentElement;
    }
    createMessage(text) {
        this._text.innerText = text;
        this.getEl('.search-search_node_elementh2').insertAdjacentElement('afterend',this._text );
    }
    moveTo(toNode) {
        node.classList.remove('node');
        node = toNode;
        node.scrollIntoView();
        node.classList.add('node');
        this.checkNode(node);
        this.createMessage(node.className.replace('node', '') || node.tagName);
    }
    handleEvent(e) {
        switch (e.target.className) {
            case 'search-prevbutton': {
                this.moveTo(node.previousElementSibling);
                break;
            }
            case 'search-nextbutton': {
                this.moveTo(node.nextElementSibling);
                break;  
            }
            case 'search-parentbutton': {
                this.moveTo(node.parentElement);
                break;
            }
            case 'search-childrenbutton': {
                this.moveTo(node.children[0]);
                break;
            }
            case 'search-searchbutton': {
                e.preventDefault();
                try {
                    let result = this.getEl('.search-input').value;
                    node = document.querySelector(result);
                    node.scrollIntoView();
                    node.classList.add('node');
                    this.checkNode(node);
                    this.createMessage(node.className.replace('node', '') || node.tagName);
                } catch (error) {
                    this.createMessage(`There isnt selector`);
                }  
                break; 
            }
            case 'search-xbutton': {
                this.remove();
                break;
            }
            default: {}
        }
    }
}
customElements.get('selector-form') || customElements.define('selector-form', SelectorForm);
document.body.insertAdjacentElement('afterbegin', document.createElement('selector-form'));



